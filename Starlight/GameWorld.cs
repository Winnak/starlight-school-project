﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace Starlight
{
    class GameWorld
    {
        #region Fields
        private readonly Graphics graphics;
        private readonly BufferedGraphics buffer;

        private float currentFPS;
        private DateTime lastFrameStarted;

        private Player player;
        private Scene currScene;
        private List<Light> lightObjects;
        private ShadowManager shadowLayer;

        private bool gameOver;
        public Pathfinding pathfinder;
        #endregion

        #region Constructer
        public GameWorld(Graphics graphics, Rectangle windowSize)
        {
            buffer = BufferedGraphicsManager.Current.Allocate(
                graphics, windowSize);
            this.graphics = buffer.Graphics;

            SetupWorld();
        }
        #endregion

        private void SetupWorld()
        {
            //TODO: clean this mess up

            currScene = new Scene(@"Levels\pathingtest.txt");

            pathfinder = new Pathfinding(currScene.WorldNodes, currScene.WorldWalls);

            player = new Player(@"Assets\player.png", new PointF(320, 240),
                currScene.WorldWalls);
            Enemy enemy1 = new Enemy(@"Assets\Nazi.png", new PointF(160, 200), player, pathfinder);

            currScene.WorldObjects.Add(enemy1);
            
            lightObjects = new List<Light>
            {
                //new Light(player),
                new Light(enemy1)
            };

            foreach(Light light in lightObjects)
            {currScene.WorldObjects.Add(light);}

            currScene.WorldObjects.Add(player);

            //Add all the initialized lights to the shadow manager.
            shadowLayer = new ShadowManager(lightObjects);

            gameOver = false;

            //Reset when game sets up.
            lastFrameStarted = DateTime.Now;
        }

        public void GameLoop()
        {
            //Calculate time spend since last game loop
            TimeSpan deltaTime = DateTime.Now - lastFrameStarted;
            int milliSeconds = deltaTime.Milliseconds > 0 ?
                deltaTime.Milliseconds : 1;
            currentFPS = 1000 / milliSeconds;
            //Set new frame start 
            lastFrameStarted = DateTime.Now;

            graphics.Clear(Color.Black);
            //Sprite sheets fuck up if translating with floating points
            graphics.TranslateTransform(
                Convert.ToInt32(320 - player.Position.X),
                Convert.ToInt32(240 - player.Position.Y));

            shadowLayer.Draw(graphics);
            shadowLayer.Reset(graphics);

            Update();
            GlobalDraw();
            graphics.ResetTransform();

            if(gameOver)
            {
                currScene.Dispose();
            }
        }
        /// <summary>
        /// Updates objects
        /// </summary>
        private void Update()
        {
            shadowLayer.Draw(graphics);
            foreach (GameObject obj in currScene.WorldObjects)
            {
                obj.Update(currentFPS);
                obj.Draw(graphics); //combined the draw and update to spare a loop.
            }
        }

        /// <summary>
        /// Draws game world related stuff
        /// </summary>
        private void GlobalDraw()
        {
            #region Debug-stuff
            #if DEBUG
            //Draw FPS counter
            Font font = new Font("Arial", 16);
            Brush brush = new SolidBrush(Color.White);
            graphics.DrawString(string.Format("FPS: {0}", currentFPS),
                font, brush, new PointF(player.Position.X-320, player.Position.Y-240));
            #endif
            #endregion

            buffer.Render();
        }
    }
}

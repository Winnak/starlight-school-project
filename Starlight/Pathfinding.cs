﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace Starlight
{
    class Pathfinding
    {
        private readonly Queue<PointF> orderQue; //The path to the goal
        private readonly List<Node> nodes; //Pathing nodes from the level
        private List<GameObject> blocks; //List of all objects that can block the path

        /// <summary>
        /// In case that map change and new obstacles come
        /// </summary>
        public List<GameObject> Blocks
        {
            set { this.blocks = value; }
            get { return this.blocks; }
        }

        public Pathfinding(List<Node> nodes, List<GameObject> blocks)
        {
            this.nodes = nodes;
            this.blocks = blocks;
            this.orderQue = new Queue<PointF>();
            if(nodes.Count == 0)
            {
                throw new Exception(
                    "No nodes added to pathfinder, cannot run without");
            }
        }

        /// <summary>
        /// Finds the path between to points
        /// </summary>
        /// <param name="start">Current position of the object needing directions</param>
        /// <param name="goal">The position the object want to go to</param>
        /// <returns>Directions to the goal from the start</returns>
        public Queue<PointF> FindPath(PointF start, PointF goal)
        {
            Point igoal = new Point(Convert.ToInt32(goal.X),
                Convert.ToInt32(goal.Y));
            List<Node> lockedNodes = new List<Node>();
            List<Node> openNodes = nodes;
            openNodes.Add(new Node(igoal.X, igoal.Y));

            //calculate all the H scores outside the loop (they don't change)
            foreach(Node node in openNodes)
            {
                node.H = Math.Abs(node.Position.X - goal.X)
                         + Math.Abs(node.Position.Y - goal.Y);
            }
            while(
                Convert.ToInt32(start.X) != igoal.X &&
                Convert.ToInt32(start.Y) != igoal.Y)
            {
                foreach (Node node in openNodes)
                {
                    #region Start Nodes
                    bool startCollided = false;
                    foreach(GameObject block in blocks)
                    {
                        if(LineCollision(start, node.Position, block))
                        {
                            startCollided = true;
                        }
                    }
                    if(!(startCollided))
                    {
                        node.G = Math.Abs(node.Position.X - start.X) 
                            + Math.Abs(node.Position.Y - start.Y);
                    }

                    #endregion
                }

                //Find the next node by finding the node with the lowest score
                float tempFScore = openNodes[0].F;
                Node nextNode = new Node(0, 0);
                foreach (Node node in openNodes)
                {
                    if(lockedNodes.Contains(node))
                    {
                        continue;
                    }
                    if(node.F < tempFScore)
                    {
                        tempFScore = node.F;
                        nextNode = node;
                    }
                }
                openNodes.Remove(nextNode); // Leave now
                lockedNodes.Add(nextNode); // And never come back. :'(
                orderQue.Enqueue(nextNode.Position);


                start = nextNode.Position;
            }
            return orderQue;
        }

        /// <summary>
        /// Creates a line between to points and checks for collisions along the way.
        /// </summary>
        /// <param name="start">Start point of the line</param>
        /// <param name="end">End point of the line</param>
        /// <param name="box">Collisionbox to check against</param>
        /// <returns>Colliding</returns>
        private bool LineCollision(PointF start, PointF end, GameObject box)
        {
            int colliding = 0;

            // form Ax + By = C
            float A = start.Y/start.X;
            float C = start.Y - end.Y;
            float B = (A*start.X - C)/start.Y;
                
            //Box top line: 0x+y=C
            if (box.CollisionBox.Left>
                DeterminantMethod(false, A, B, C, 0, 1, box.CollisionBox.Top) &&
                DeterminantMethod(false, A, B, C, 0, 1, box.CollisionBox.Top) >
                box.CollisionBox.Right)
            {
                colliding += 1;
            }

            //Box left line: x+0y=C
            if (box.CollisionBox.Top >
                DeterminantMethod(false, A, B, C, 1, 0, box.CollisionBox.Left) &&
                DeterminantMethod(false, A, B, C, 1, 0, box.CollisionBox.Left) >
                box.CollisionBox.Bottom)
            {
                colliding += 1;
            }

            //Box right line: x+0y=C
            if (box.CollisionBox.Top >
                DeterminantMethod(false, A, B, C, 1, 0, box.CollisionBox.Right) &&
                DeterminantMethod(false, A, B, C, 1, 0, box.CollisionBox.Right) >
                box.CollisionBox.Bottom)
            {
                colliding += 1;
            }

            //Box bottom line: 0x+y=C
            if (box.CollisionBox.Left >
                DeterminantMethod(false, A, B, C, 0, 1, box.CollisionBox.Bottom) &&
                DeterminantMethod(false, A, B, C, 0, 1, box.CollisionBox.Bottom) >
                box.CollisionBox.Right)
            {
                colliding += 1;
            }

            return colliding == 2;
        }

        /// <summary>
        /// Calculates:
        /// |c1, b1|   |a1, c1|
        /// |c2, b2|   |a2, c2|
        /// -------- , --------
        /// |a1, b1|   |a1, b1|
        /// |a2, b2|   |a2, b2|
        /// </summary>
        /// <param name="xy">returns x or y</param>
        /// <param name="a1">[A]x+By=C</param>
        /// <param name="b1">Ax+[B]y=C</param>
        /// <param name="c1">Ax+By=[C]</param>
        /// <param name="a2">[A]x+By=C</param>
        /// <param name="b2">Ax+[B]y=C</param>
        /// <param name="c2">Ax+By=[C]</param>
        /// <returns>Determinant</returns>
        private float DeterminantMethod(bool xy,
            float a1, float b1, float c1, 
            float a2, float b2, float c2)
        {
            float determinant = a1 * b2 - a2 * b1;
            if(xy)
            {
                return (c1 * b2 - c2 * b1) / determinant;
            }
            return (a1 * c2 - a2 * c1) / determinant;
        }
    }
}

﻿using System;
using System.Windows.Forms;

namespace Starlight
{
    public partial class MainWindow : Form
    {
        GameWorld gameWorld;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void MainWindow_Load(object sender, EventArgs e)
        {
        }

        private void WindowTimer_Tick(object sender, EventArgs e)
        {
            if (gameWorld == null)
            {
                gameWorld = new GameWorld(CreateGraphics(), DisplayRectangle);
            }
            Keyboard.IsKeyDown(Keys.A);
            gameWorld.GameLoop();

        }
    }
}

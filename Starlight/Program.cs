﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using IrrKlang;

        //private readonly ISoundEngine soundManager = new ISoundEngine();

namespace Starlight
{
    static class Program
    {

        public static readonly ISoundEngine Sound = new ISoundEngine();
        private static List<SoundWave> _currentlyPlaying;
        public static List<SoundWave> CurrentlyPlaying
        {
            get { return _currentlyPlaying; }
            set { _currentlyPlaying = value; }
        }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //Audio stuff
            _currentlyPlaying = new List<SoundWave>();
            Sound.Default3DSoundMaxDistance = 10000;
            Sound.Default3DSoundMinDistance = 5;

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainWindow());
        }
    }
}

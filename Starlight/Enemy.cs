﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace Starlight
{
    class Enemy: Character
    {
        #region Fields
        private readonly Player player;
        private int speed = 30;

        private readonly Pathfinding pathfinder;
        private Queue<PointF> orders;
        private PointF currentOrder;
        #endregion

        public Enemy(string imagePath, PointF position,Player player,
            Pathfinding pathfinder) : base(imagePath, position)
        {
            orders = new Queue<PointF>();
            this.pathfinder = pathfinder;
            this.currentOrder = this.position;
            this.player = player;
        }

        public override void Update(float fps)
        {
            if(Convert.ToInt32(currentOrder.X) == Convert.ToInt32(position.X) &&
                Convert.ToInt32(currentOrder.Y) == Convert.ToInt32(position.Y))
            {
                if(orders.Count != 0)
                {
                    currentOrder = orders.Dequeue();
                }
                else
                {
                    orders = pathfinder.FindPath(position, player.Position);
                }
            }
            else
            {
                if(orders.Count != 0)
                {
                    this.speed = this.player.Spotted ? 60 : 30;
                    #region collisions x
                    //float x, y1, y2;
                    //if(Math.Sign(currentOrder.X - position.X) == -1)
                    //{
                    //    x = CollisionBox.Right + speed / fps;
                    //    y1 = CollisionBox.Top;
                    //    y2 = CollisionBox.Bottom;
                    //}
                    //else
                    //{
                    //    x = CollisionBox.Left - speed / fps;
                    //    y1 = CollisionBox.Top;
                    //    y2 = CollisionBox.Bottom;
                    //}

                    //bool canMoveX = true;

                    //    foreach (Wall obj in pathfinder.Blocks)
                    //    {
                    //        if (obj.PointInCollisionBox(x, y1) ||
                    //            obj.PointInCollisionBox(x, y2))
                    //        {
                    //            canMoveX = false;
                    //            break;
                    //        }
                    //    }
                    #endregion
                    //if (canMoveX)
                    //{
                        position.X +=
                            Math.Sign(currentOrder.X - position.X) * speed / fps;
                    //}

                    #region collisions y
                    //float x1, x2, y;
                    //if(Math.Sign(currentOrder.Y - position.Y) == -1)
                    //{
                    //    x1 = CollisionBox.Left;
                    //    x2 = CollisionBox.Right;
                    //    y = CollisionBox.Bottom + speed / fps;
                    //}
                    //else
                    //{
                    //    x1 = CollisionBox.Left;
                    //    x2 = CollisionBox.Right;
                    //    y = CollisionBox.Top - speed / fps;
                    //}

                    //bool canMoveY = true;

                    //foreach (Wall obj in pathfinder.Blocks)
                    //{
                    //    if (obj.PointInCollisionBox(x1, y) ||
                    //        obj.PointInCollisionBox(x2, y))
                    //    {
                    //        canMoveY = false;
                    //        break;
                    //    }
                    //}
                    #endregion
                    //if (canMoveY == false)
                    //{
                        position.Y +=
                            Math.Sign(currentOrder.Y - position.Y) * speed / fps;
                    //}

                    Angle = (float)(Math.Atan2(currentOrder.Y - position.Y, 
                        currentOrder.X - position.X) * 180 / Math.PI + 270);
                }
                else
                {
                    if (player.Spotted)
                    {
                        orders = pathfinder.FindPath(position, player.Position);
                    }
                }
            }
            if (IsCollidingWith(player))
            {
                //throw new Exception("dead");
            }
        }
    }
}

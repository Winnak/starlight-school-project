﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Starlight
{
    class Scene : IDisposable
    {
        #region Private fields
        //Header variables
        private int width; //max width of the level
        private int height; //max hight of the level
        private int tileWidth; //size of a single tile
        private int tileHeight;

        //Tile set
        private SpriteSheet spritesheet;

        //GameObject lists
        private List<GameObject> worldObjects;


        //Level file
        private readonly StreamReader levelFile;
        #endregion

        #region Properties
        internal List<GameObject> WorldObjects
        {
            get { return worldObjects; }
            set { worldObjects = value; }
        }
        public List<GameObject> WorldWalls { get; set; }
        public List<GameObject> WorldFloors { get; set; }
        public List<GameObject> WorldDoors { get; set; }
        public List<Node> WorldNodes { get; set; }

        public int TileHeight
        {
            get { return tileHeight; }
        }
        public int TileWidth
        {
            get { return tileWidth; }
        }
        #endregion

        /// <summary>
        /// Creates a level in the game, from a text document.
        /// Levels can be created with Tiled http://www.mapeditor.org (export as Flare Map (.txt))
        /// </summary>
        /// <param name="levelFile">Level file</param>
        public Scene(string levelFile)
        {
            this.levelFile = new StreamReader(levelFile);
            //convert level file to a string array
            string[] levelFileArray = this.levelFile.ReadToEnd().Split('\r');
            
            //Remove escape sequences
            for (int i = 1; i < levelFileArray.Length; i++)
            {   //index starts at 1 because the first line does not contain the escape sequence
                levelFileArray[i] = levelFileArray[i].Substring(1);
            }

            worldObjects = new List<GameObject>();
            this.WorldWalls = new List<GameObject>();
            this.WorldFloors = new List<GameObject>();
            this.WorldDoors = new List<GameObject>();
            this.WorldNodes = new List<Node>();
            GenerateMap(levelFileArray);
            Dispose();
        }

        #region Private Methods
        /// <summary>
        /// Adds GameObjects to the lists used by the GameWorld
        /// </summary>
        /// <param name="file">File to generate from</param>
        private void GenerateMap(string[] file)
        {
            for (int n = 0; n < file.Length; n++)
            {
                if (file[n] == "[header]")
                {
                    //Format of the lines are: "width=8", therefore we subtract "width="
                    width = Convert.ToInt32(file[n + 1].Substring(6));
                    height = Convert.ToInt32(file[n + 2].Substring(7));
                    tileWidth = Convert.ToInt32(file[n + 3].Substring(10));
                    tileHeight = Convert.ToInt32(file[n + 4].Substring(11));
                }
                if (file[n] == "[tilesets]")
                {
                    //This line has all parameters in one line, comma-separated
                    string[] tilesetparams = file[n + 1].Substring(8).Split(',');

                    spritesheet = new SpriteSheet(tilesetparams[0],
                        Convert.ToInt32(tilesetparams[1]),
                        Convert.ToInt32(tilesetparams[2]),
                        Convert.ToInt32(tilesetparams[3]),
                        Convert.ToInt32(tilesetparams[4]));
                }
                if (file[n] == "[layer]")
                {
                    #region Wall
                    if (file[n + 1] == "type=Wall")
                    {
                        for (int y = 0; y < height; y++)
                        {
                            string[] levelRow = file[n + 3 + y].Split(',');
                            for (int x = 0; x < width; x++)
                            {
                                int levelCol;
                                bool success = Int32.TryParse(levelRow[x], out levelCol);
                                if (success && levelCol != 0)
                                {
                                    Wall obj = new Wall(levelCol - 1,
                                        x * spritesheet.SpriteSheetTileWidth,
                                        y * spritesheet.SpriteSheetTileHeight);
                                    worldObjects.Add(obj);
                                    this.WorldWalls.Add(obj);
                                }

                            }
                        }
                    }
                    #endregion
                    #region Floor
                    if (file[n + 1] == "type=Floor")
                    {
                        for (int y = 0; y < height; y++)
                        {
                            string[] levelRow = file[n + 3 + y].Split(',');
                            for (int x = 0; x < width; x++)
                            {
                                int levelCol;
                                bool success = Int32.TryParse(levelRow[x], out levelCol);
                                if (success && levelCol != 0)
                                {
                                    Floor obj = new Floor(levelCol - 1,
                                        x * spritesheet.SpriteSheetTileWidth,
                                        y * spritesheet.SpriteSheetTileHeight);
                                    worldObjects.Add(obj);
                                    this.WorldFloors.Add(obj);
                                }
                            }
                        }
                    }
                    #endregion
                    #region Door
                    if (file[n + 1] == "type=Door")
                    {
                        for (int y = 0; y < height; y++)
                        {
                            string[] levelRow = file[n + 3 + y].Split(',');
                            for (int x = 0; x < width; x++)
                            {
                                int levelCol;
                                bool success = Int32.TryParse(levelRow[x], out levelCol);
                                if (success && levelCol != 0)
                                {
                                    Door obj = new Door(levelCol - 1,
                                        x * spritesheet.SpriteSheetTileWidth,
                                        y * spritesheet.SpriteSheetTileHeight);
                                    worldObjects.Add(obj);
                                    this.WorldDoors.Add(obj);
                                }
                            }
                        }
                    }
                    #endregion
                    #region Nodes
                    if (file[n + 1] == "type=Nodes")
                    {
                        for (int y = 0; y < height; y++)
                        {
                            string[] levelRow = file[n + 3 + y].Split(',');
                            for (int x = 0; x < width; x++)
                            {
                                int levelCol;
                                bool success = Int32.TryParse(levelRow[x], out levelCol);
                                if (success && levelCol != 0)
                                {
                                    Node obj = new Node(
                                        x * spritesheet.SpriteSheetTileWidth,
                                        y * spritesheet.SpriteSheetTileHeight);
                                    this.WorldNodes.Add(obj);
                                }
                            }
                        }
                    }
                    #endregion
                }
            }
        }
        #endregion

        public void Dispose()
        {
            this.levelFile.DiscardBufferedData();
            this.levelFile.Close();
            this.levelFile.Dispose();
        }
    }
}
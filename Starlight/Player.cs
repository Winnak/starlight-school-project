﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms; //Required for Keys-enum

namespace Starlight
{
    class Player : Character
    {
        private const int Speed = 64;
        private readonly List<GameObject> objects;
        private readonly List<SoundWave> emittedSounds;
        private readonly Sound walkingSound;
        private bool spotted;
        public bool Spotted
        {
            get { return this.spotted; }
            set { this.spotted = value; }
        }

        public Player(string fileName, PointF startPosition, 
            List<GameObject> objects) : base(fileName, startPosition)
        {
            this.objects = objects;
            this.position = startPosition;
            walkingSound = new Sound(@"Assets\footsteps.wav", position);
            emittedSounds = new List<SoundWave>();
        }

        public override RectangleF CollisionBox
        {
            get
            {
                return new RectangleF(position, new Size(20,20));
            }
        }

        public override void Update(float fps)
        {
            bool moving = false;
            #region Input
            #region W
            if (Keyboard.IsKeyDown(Keys.W))
            {
                float x1 = CollisionBox.Left;
                float x2 = CollisionBox.Right;
                float y = CollisionBox.Top - Speed / fps;

                bool canMove = true;

                foreach (Wall obj in objects)
                {
                    if (obj.PointInCollisionBox(x1, y) ||
                        obj.PointInCollisionBox(x2, y))
                    {
                        canMove = false;
                    }
                }

                if (canMove)
                {
                    moving = true;
                    position.Y -= Speed / fps;
                }
            }
            #endregion
            #region A
            if (Keyboard.IsKeyDown(Keys.A))
            {
                float x = CollisionBox.Left - Speed / fps;
                float y1 = CollisionBox.Top;
                float y2 = CollisionBox.Bottom;

                bool canMove = true;

                foreach (Wall obj in objects)
                {
                    if (obj.PointInCollisionBox(x, y1) ||
                        obj.PointInCollisionBox(x, y2))
                    {
                        canMove = false;
                    }
                }

                if (canMove)
                {
                    moving = true;
                    position.X -= Speed / fps;
                }
            }
            #endregion
            #region S
            if (Keyboard.IsKeyDown(Keys.S))
            {
                float x1 = CollisionBox.Left;
                float x2 = CollisionBox.Right;
                float y = CollisionBox.Bottom + Speed / fps;

                bool canMove = true;

                foreach (Wall obj in objects)
                {
                    if (obj.PointInCollisionBox(x1, y) ||
                        obj.PointInCollisionBox(x2, y))
                    {
                        canMove = false;
                    }
                }

                if (canMove)
                {
                    moving = true;
                    position.Y += Speed / fps;
                }
            }
            #endregion
            #region D
            if (Keyboard.IsKeyDown(Keys.D))
            {
                float x = CollisionBox.Right + Speed / fps;
                float y1 = CollisionBox.Top;
                float y2 = CollisionBox.Bottom;

                bool canMove = true;

                foreach (Wall obj in objects)
                {
                    if (obj.PointInCollisionBox(x, y1) ||
                        obj.PointInCollisionBox(x, y2))
                    {
                        canMove = false;
                    }
                }

                if (canMove)
                {
                    moving = true;
                    position.X += Speed / fps;
                }
            }
            #endregion
#if DEBUG
            #region Spotted

            if(Keyboard.IsKeyDown(Keys.Space))
            {
                this.Spotted = true;
            }
            #endregion
#endif
            #endregion
            #region Sound
            if(walkingSound.SoundControl == null)
            {
                walkingSound.Play();
            }
            if (moving && walkingSound.SoundControl.Finished)
            {
                //Program.CurrentlyPlaying.Remove(walkingSound);
                SoundWave sndwav = new SoundWave(
                    new PointF(position.X + 10, position.Y + 10),
                    0.25f);
                emittedSounds.Add(sndwav);
                walkingSound.Play();
                Program.CurrentlyPlaying.Add(sndwav);
            }
            if (!moving)
            {
                walkingSound.SoundControl.Stop();
                //Program.CurrentlyPlaying.Remove(walkingSound);
            }
            foreach(SoundWave sndwave in emittedSounds)
            {
                if(sndwave.State == SoundState.Plays)
                {
                    sndwave.Update(fps);
                }
            }
            #endregion
        }
        public override void Draw(Graphics graphics)
        { 
            foreach(SoundWave sndwave in emittedSounds)
            {
                if(sndwave.State == SoundState.Plays)
                {
                    sndwave.Draw(graphics);
                }
            }
            graphics.DrawImage(spriteSheet.SpriteImage, position.X, position.Y);
        }
    }
}

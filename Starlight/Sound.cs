﻿using System.Drawing;
using IrrKlang;

namespace Starlight
{
    /// <summary>
    /// Creates sound waves and plays a sound
    /// </summary>
    class Sound
    {
        private ISound sound;
        private PointF position;
        private readonly ISoundSource soundSource;

        public ISound SoundControl
        {
            get { return this.sound; }
            set { this.sound = value; }
        }
        public PointF Position
        {
            get { return this.position; }
            set { this.position = value; }
        }

        /// <summary>
        /// Plays a sound and emits a sound wave from the source. Use this if it 
        /// is not the player emitting the sound
        /// </summary>
        /// <param name="filePath">Path to the sound</param>
        /// <param name="position">Position of the emitting object</param>
        /// <param name="playerPos">Position of the player</param>
        public Sound(string filePath, PointF position, PointF playerPos)
        {
            this.soundSource = Program.Sound.AddSoundSourceFromFile(filePath);
            this.sound = Program.Sound.Play3D(soundSource,
                position.X - playerPos.X,
                position.Y - playerPos.Y,
                0, false, false, false);
            this.Position = position;
        }
        public Sound(string filePath, PointF playerPos)
        {
            this.soundSource = Program.Sound.AddSoundSourceFromFile(filePath);
            this.Position = playerPos;
        }

        public void Play()
        {
            this.sound = Program.Sound.Play2D(soundSource, false, false, false);
        }
    }
}

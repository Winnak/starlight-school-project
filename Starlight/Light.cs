﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace Starlight
{
    /// <summary>
    /// Contains the path data of a light source
    /// </summary>
    class Light : GameObject, IDisposable
    {
        private SizeF boxAreaOfCone;
        private SizeF boxAreaOfEmitter;
        private readonly float startAngle;
        private readonly float sweepAngle;
        private GraphicsPath path;
        private GraphicsPath innerPath;
        private readonly GameObject attachedTo;

        public GraphicsPath Path
        {
            get { return this.path; }
            set { this.path = value; }
        }
        public GraphicsPath InnerPath
        {
            get { return this.innerPath; }
            set { this.innerPath = value; }
        }
        public SizeF BoxAreaOfCone
        {
            get { return this.boxAreaOfCone; }
            set { this.boxAreaOfCone = value; }
        }
        public SizeF BoxAreaOfEmitter
        {
            get { return this.boxAreaOfEmitter; }
            set { this.boxAreaOfEmitter = value; }
        }

        /// <summary>
        /// Creates a flashlight for an enemy
        /// </summary>
        /// <param name="attach">Enemy with a flashlight</param>
        public Light(Enemy attach)
        {
            this.attachedTo = attach;
            this.boxAreaOfCone = new SizeF(192, 192);
            this.boxAreaOfEmitter = new SizeF(30, 30);
            this.startAngle = 70;
            this.sweepAngle = 40;
            this.path = new GraphicsPath();
            this.innerPath = new GraphicsPath();
        }
        /// <summary>
        /// Players own flashlight
        /// </summary>
        /// <param name="attach">Player with a flashlight</param>
        public Light(Player attach)
        {
            this.attachedTo = attach;
            this.boxAreaOfEmitter = new SizeF(100, 100);
            this.path = new GraphicsPath();
            this.innerPath = new GraphicsPath();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="postion"></param>
        /// <param name="size"></param>
        public Light(PointF postion, float size)
        {
            this.attachedTo = new Character(position);
            this.boxAreaOfEmitter = new SizeF(size, size);
            this.path = new GraphicsPath();
            this.innerPath = new GraphicsPath();
        }

        public override void Draw(Graphics graphics)
        {
            this.position = new PointF(
                // + 10 because characters are 20 pixel wide
                this.attachedTo.Position.X - boxAreaOfEmitter.Width / 2 + 10,
                this.attachedTo.Position.Y - boxAreaOfEmitter.Height / 2 + 10);

            this.path.Reset();
            this.innerPath.Reset();
            this.innerPath.AddEllipse(new RectangleF(position, boxAreaOfEmitter));

            this.path.AddLine(
                position.X + 16,
                position.Y + 16,
                position.X + 16,
                position.Y + 16);

            if (!(attachedTo is Player))
            {
            this.path.AddArc(
                new RectangleF(
                    new PointF(position.X - boxAreaOfCone.Width / 2,
                               position.Y + 6 - boxAreaOfCone.Height / 2),
                    boxAreaOfCone),
                startAngle + this.attachedTo.Angle,
                sweepAngle);   
            }
        }

        public void Dispose()
        {
            path.Reset();
            innerPath.Reset();
            path.Dispose();
            innerPath.Dispose();
        }
    }
}

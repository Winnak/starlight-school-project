﻿using System.Drawing;

namespace Starlight
{
    class SpriteSheet
    {
        #region Private fields
        private int spriteSheetTileWidth;
        private int spriteSheetTileHeight;
        private int spriteSheetOffsetX;
        private int spriteSheetOffsetY;

        private Image spriteImage;
        private Rectangle[,] frame;
        #endregion

        #region Properties
        public string SpriteSheetFile { get; set; }
        public int SpriteSheetTileWidth
        {
            get { return spriteSheetTileWidth; }
            set { spriteSheetTileWidth = value; }
        }
        public int SpriteSheetTileHeight
        {
            get { return spriteSheetTileHeight; }
            set { spriteSheetTileHeight = value; }
        }
        public int SpriteSheetOffsetX
        {
            get { return spriteSheetOffsetX; }
            set { spriteSheetOffsetX = value; }
        }
        public int SpriteSheetOffsetY
        {
            get { return spriteSheetOffsetY; }
            set { spriteSheetOffsetY = value; }
        }
        public Image SpriteImage
        {
            get { return spriteImage; }
            set { spriteImage = value; }
        }
        public Rectangle[,] Frame
        {
            get { return frame; }
            set { frame = value; }
        }

        public Rectangle[] FrameList { get; set; }
        #endregion

        #region Constructer
        /// <summary>
        /// Creates an array of sprites out of an image 
        /// </summary>
        /// <param name="fileName">file path to the image</param>
        public SpriteSheet(string fileName)
        {
            spriteImage = Image.FromFile(fileName);
            spriteSheetTileWidth   = 32;
            spriteSheetTileHeight  = 32;
            spriteSheetOffsetX     = 0;
            spriteSheetOffsetY     = 0;

            frame = SheetToFrames(spriteImage,
                spriteSheetOffsetX, spriteSheetOffsetY);

            FrameList = MultiToSingleArray(frame);
        }
        /// <summary>
        /// Creates an array of sprites out of an image 
        /// </summary>
        /// <param name="fileName">file path to the image</param>
        /// <param name="spriteSheetTileWidth">width of a single frame</param>
        /// <param name="spriteSheetTileHeight">height of a single frame</param>
        public SpriteSheet(string fileName,
            int spriteSheetTileWidth, int spriteSheetTileHeight)
        {
            spriteImage = Image.FromFile(fileName);
            this.spriteSheetTileWidth = spriteSheetTileWidth;
            this.spriteSheetTileHeight = spriteSheetTileHeight;
            spriteSheetOffsetX = 0;
            spriteSheetOffsetY = 0;

            frame = SheetToFrames(spriteImage,
                spriteSheetOffsetX, spriteSheetOffsetY);

            FrameList = MultiToSingleArray(frame);
        }
        /// <summary>
        /// Creates an array of sprites out of an image 
        /// </summary>
        /// <param name="fileName">file path to the image</param>
        /// <param name="spriteSheetTileWidth">width of a single frame</param>
        /// <param name="spriteSheetTileHeight">height of a single frame</param>
        /// <param name="spriteSheetOffsetX">margin to the left of the first frame in pixels</param>
        /// <param name="spriteSheetOffsetY">margin over the first frame in pixels</param>
        public SpriteSheet(string fileName,
            int spriteSheetTileWidth, int spriteSheetTileHeight,
            int spriteSheetOffsetX, int spriteSheetOffsetY)
        {
            spriteImage = Image.FromFile(fileName);
            this.spriteSheetTileWidth = spriteSheetTileWidth;
            this.spriteSheetTileHeight = spriteSheetTileHeight;
            this.spriteSheetOffsetX = spriteSheetOffsetX;
            this.spriteSheetOffsetY = spriteSheetOffsetY;

            frame = SheetToFrames(spriteImage,
                this.spriteSheetOffsetX, this.spriteSheetOffsetY);

            FrameList = MultiToSingleArray(frame);
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Takes an image and splits it into a lot of frames
        /// </summary>
        /// <param name="sprite">Image to split</param>
        /// <param name="offsetX">Amount of pixels to the left of the first frame</param>
        /// <param name="offsetY">Amount of pixels over the first frame</param>
        /// <returns>A multi dimensional array of frames</returns>
        private Rectangle[,] SheetToFrames(Image sprite, int offsetX, int offsetY)
        {
            var slackX = 0;
            var slackY = 0;

            //Check for excess pixels
            if ((sprite.Width - offsetX) % spriteSheetTileWidth != 0)
            {
                slackX = (sprite.Width - offsetX) % spriteSheetTileWidth;
            }
            if ((sprite.Width - offsetY) % spriteSheetTileHeight != 0)
            {
                slackY = (sprite.Width - offsetY) % spriteSheetTileHeight;
            }

            Rectangle[,] frames = new Rectangle[
                (sprite.Width - offsetX - slackX)/spriteSheetTileWidth,
                (sprite.Height - offsetY - slackY) / spriteSheetTileHeight];

            for (int y = 0; y < frames.GetLength(1); y++)
            {
                for (int x = 0; x < frames.GetLength(0); x++)
                {
                    frames[x, y] = new Rectangle(
                        x * spriteSheetTileWidth, y * spriteSheetTileHeight,
                        spriteSheetTileWidth, spriteSheetTileHeight);
                }
            }

            return frames;
        }

        /// <summary>
        /// Takes the frames multi-array, and turns it into a single array
        /// </summary>
        /// <param name="frames">The multi-array containing all the frames</param>
        /// <returns>An array of frames</returns>
        public Rectangle[] MultiToSingleArray(Rectangle[,] frames)
        {
            Rectangle[] listOfFrames = new Rectangle[
                frames.GetLength(0) * frames.GetLength(1)];

            int z = 0;
            for (int y = 0; y < frames.GetLength(1); y++)
            {
                for (int x = 0; x < frames.GetLength(0); x++)
                {
                    listOfFrames[z] = frames[x, y];
                    z++;
                }
            }

            return listOfFrames;
        }
        #endregion

    }
}

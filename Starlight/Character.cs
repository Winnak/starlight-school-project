﻿using System.Drawing;

namespace Starlight
{
    class Character : GameObject
    {
        private float angle;
        public override float Angle
        {
            get { return angle; }
            set { angle = value; }
        }

        public Character(string imagePath, PointF startPosition) 
            : base(imagePath, startPosition)
        {

        }

        public Character(PointF startPosition)
            : base(startPosition)
        { 
            
        }

        public override void Update(float fps)
        {
            if (angle > 360)
            {
                angle = 0;
            }
            base.Update(fps);
        }
    }
}

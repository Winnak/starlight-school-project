﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace Starlight
{
    class Menu
    {
        public int chosen = 0;

        public int Chosen
        {
            get {return chosen;}
            set {chosen = value;}
        }

        
        public void KeyReader()
        {

            if (Keyboard.IsKeyDown(Keys.W))
            {
                if (chosen ==2)
                {
                    chosen = 0;
                }

                else
                {   
                    chosen++;
                }
            }

            else if (Keyboard.IsKeyDown(Keys.S))
            {
                if (chosen ==0)
                {   
                    chosen = 2;
                }

                else
                {
                    chosen--;
                }
            }

        }

        public void Enter()
        {
            if (Keyboard.IsKeyDown(Keys.Enter))
            {
                if (chosen == 0)
                { 
                    
                }
                else if (chosen == 1)
                {
                    //Story
                }
                else if (chosen == 2)
                { 
                
                }
            }
        }


        public override void Draw(Graphics graphics)
        {
            graphics.DrawImage(Image.FromFile(@"/menu baggrund"), 0, 0);
            if (chosen == 0)
            {
                graphics.DrawString("Start Game", new Font("Arial", 14f), new SolidBrush(Color.Red), new PointF(230, 100));
                graphics.DrawString("See Story", new Font("Arial", 14f), new SolidBrush(Color.White), new PointF(230, 200));
                graphics.DrawString("Quit", new Font("Arial", 14f), new SolidBrush(Color.White), new PointF(230, 300));
                    
            }
            else if (chosen == 1)
            {
                graphics.DrawString("Start Game", new Font("Arial", 14f), new SolidBrush(Color.White), new PointF(230, 100));
                graphics.DrawString("See Story", new Font("Arial", 14f), new SolidBrush(Color.Red), new PointF(230, 200));
                graphics.DrawString("Quit", new Font("Arial", 14f), new SolidBrush(Color.White), new PointF(230, 300));

            }
            else if (chosen == 2)
            {
                graphics.DrawString("Start Game", new Font("Arial", 14f), new SolidBrush(Color.White), new PointF(230, 100));
                graphics.DrawString("See Story", new Font("Arial", 14f), new SolidBrush(Color.White), new PointF(230, 200));
                graphics.DrawString("Quit", new Font("Arial", 14f), new SolidBrush(Color.Red), new PointF(230, 300));

            }
        }

    }
}

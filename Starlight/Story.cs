﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace Starlight
{
    class Story
    {

        public int pageNumber = 0;

        public void Exit()
        { if (Keyboard.IsKeyDown(Keys.Escape)) { return; } }

        public void KeyReader()
        {
            if (Keyboard.IsKeyDown(Keys.D))
            {
                if (pageNumber == 4)
                { pageNumber = 0; }
                else
                { pageNumber++; }
            }
            else if (Keyboard.IsKeyDown(Keys.A))
            {
                if (pageNumber == 0)
                { pageNumber = 4; }
                else
                { pageNumber--; }
            }
        }

        public override void Draw(Graphics graphics)
        {
            if (pageNumber == 0)
            { graphics.DrawImage(Image.FromFile(@"/Intro"), 0, 0); }
            else if (pageNumber == 1)
            { graphics.DrawImage(Image.FromFile(@"/first"), 0, 0); }
            else if (pageNumber == 2)
            { graphics.DrawImage(Image.FromFile(@"/firstsecond"), 0, 0); }
            else if (pageNumber == 1)
            { graphics.DrawImage(Image.FromFile(@"/secondthird"), 0, 0); }
            else if (pageNumber == 1)
            { graphics.DrawImage(Image.FromFile(@"/thirdfourth"), 0, 0); }
        }

    }
}

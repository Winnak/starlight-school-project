﻿using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace Starlight
{
    class ShadowManager
    {
        private readonly List<Light> lights;

        public ShadowManager(List<Light> lights)
        {
            this.lights = lights;
        }

        public void Draw(Graphics graphics)
        {
#if DEBUG
            graphics.SetClip(new Rectangle(0, 0, 150, 20), CombineMode.Intersect);
#endif
#if !(DEBUG)
            graphics.SetClip(new Rectangle(0,0,1,1), CombineMode.Intersect);
#endif
            if (lights.Count != 0)
            {
                foreach (var light in this.lights)
                {
                    graphics.SetClip(light.Path, CombineMode.Union);
                    graphics.SetClip(light.InnerPath, CombineMode.Union);
                }
                foreach(SoundWave snd in Program.CurrentlyPlaying)
                {
                    if(snd.State == SoundState.Plays)
                    {
                        graphics.SetClip(snd.Path, CombineMode.Union);
                    }
                }
            }
        }

        public void Reset(Graphics graphics)
        {
            graphics.ResetClip();
        }
    }
}

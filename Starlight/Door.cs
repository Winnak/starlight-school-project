﻿using System.Drawing;

namespace Starlight
{
    class Door : GameObject
    {
       private readonly int facingIndex;

        public Door(string imagePath, int x, int y) : base(imagePath, x, y)
        {
        }
        public Door(string imagePath, int index, int x, int y)
            : base(imagePath, x, y)
        {
            position = new PointF(x, y);
            facingIndex = index;
        }
        public Door(Point position, int index)
            : base(position)
        {
            facingIndex = index;
        }
        public Door(int index, int x, int y)
            : base(x, y)
        {
            position = new PointF(x, y);
            facingIndex = index;
        }

        public override void Draw(Graphics graphics)
        {
            graphics.DrawImage(spriteSheet.SpriteImage, position.X, position.Y, 
                spriteSheet.FrameList[facingIndex], GraphicsUnit.Pixel);
        }
    }
}

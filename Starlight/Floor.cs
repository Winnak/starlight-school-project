﻿using System.Drawing;

namespace Starlight
{
    class Floor : GameObject
    {
        private readonly int facingIndex;

        public Floor(string imagePath, int x, int y) : base(imagePath, x, y)
        {

        }
        public Floor(string imagePath, int index, int x, int y)
            : base(imagePath, x, y)
        {
            position = new PointF(x, y);
            facingIndex = index;
        }
        public Floor(Point position, int index)
            : base(position)
        {
            facingIndex = index;
        }
        public Floor(int index, int x, int y)
            : base(x, y)
        {
            position = new PointF(x, y);
            facingIndex = index;
        }

        public override void Draw(Graphics graphics)
        {
            graphics.DrawImage(spriteSheet.SpriteImage, position.X, position.Y, 
                spriteSheet.FrameList[facingIndex], GraphicsUnit.Pixel);
        }
    }
}

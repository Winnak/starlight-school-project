﻿namespace Starlight
{
    class Node : GameObject
    {
        private float gScore;
        private float hScore;

        /// <summary>
        /// Sum of G and H
        /// </summary>
        public float F
        {
            get { return G + H; }
        }
        /// <summary>
        /// Distance from the start point
        /// </summary>
        public float G
        {
            get { return this.gScore; }
            set { this.gScore = value; }
        }
        /// <summary>
        /// Distance from the goal point
        /// </summary>
        public float H
        {
            get { return this.hScore; }
            set { this.hScore = value; }
        }

        public Node(int posx, int posy, float gScore, float hScore)
            : base(posx, posy)
        {
            this.gScore = gScore;
            this.hScore = hScore;
        }

        public Node(int posx, int posy) : base(posx, posy)
        {
            this.gScore = 0;
            this.hScore = 0;
        }
        public override void Draw(System.Drawing.Graphics graphics)
        {
        }
    }
}

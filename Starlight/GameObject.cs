﻿using System.Drawing;

namespace Starlight
{
    abstract class GameObject
    {
        protected SpriteSheet spriteSheet;
        protected PointF position;

        public PointF Position
        {
            get { return position; }
            set { position = value; }
        }
        public virtual float Angle { get; set; }

        protected GameObject(string fileName, PointF startPosition)
        {
            spriteSheet = new SpriteSheet(fileName, 32, 32);
            position = startPosition;
        }
        protected GameObject(string fileName, int posx, int posy)
        {
            spriteSheet = new SpriteSheet(fileName, 32, 32);
            position = new PointF(posx, posy);
        }
        protected GameObject(PointF startPosition)
        {
            position = startPosition;
            spriteSheet = new SpriteSheet(@"Assets\tileset-starlight.png", 32, 32);
        }
        protected GameObject(int posx, int posy)
        {
            position = new PointF(posx, posy);
            spriteSheet = new SpriteSheet(@"Assets\tileset-starlight.png", 32, 32);
        }

        protected GameObject()
        {
            this.position = new PointF(0, 0);
        }

        /// <summary>
        /// Provides a Collision box (rectangle) 
        /// </summary>
        public virtual RectangleF CollisionBox
        {
            get
            {
                return new RectangleF(position, new Size(
                    spriteSheet.SpriteSheetTileWidth, 
                    spriteSheet.SpriteSheetTileHeight));
            }
        }
        /// <summary>
        /// Return if the GameObject is colliding with another GameObject
        /// </summary>
        /// <param name="other">The other GameObject to test agains</param>
        /// <returns>Returns true if colliding or false if not</returns>
        protected bool IsCollidingWith(GameObject other)
        {
            //Uses intersectsWith on this objects collisionbox and the other gameobjects collisonbox
            //Returns the result
            return CollisionBox.IntersectsWith(other.CollisionBox);
        }
        public bool PointInCollisionBox(float x, float y)
        {
            return x >= CollisionBox.Left
                && x <= CollisionBox.Right
                && y >= CollisionBox.Top
                && y <= CollisionBox.Bottom;
        }
        public virtual void Update(float fps)
        {
        }
        public virtual void Draw(Graphics graphics)
        {   
            graphics.DrawImage(spriteSheet.SpriteImage, position.X, position.Y, 
                spriteSheet.FrameList[0], GraphicsUnit.Pixel);

        }
    }
}

﻿using System.Drawing;

namespace Starlight
{
    class Wall : GameObject
    {
        private readonly int facingIndex;

        public Wall(string imagePath, int x, int y) : base(imagePath, x, y)
        {

        }
        public Wall(string imagePath, int index, int x, int y)
            : base(imagePath, x, y)
        {
            position = new PointF(x, y);
            facingIndex = index;
        }
        public Wall(Point position, int index)
            : base(position)
        {
            facingIndex = index;
        }
        public Wall(int index, int x, int y)
            : base(x, y)
        {
            position = new PointF(x, y);
            facingIndex = index;
        }

        public override void Draw(Graphics graphics)
        {
            graphics.DrawImage(spriteSheet.SpriteImage, position.X, position.Y, 
                spriteSheet.FrameList[facingIndex], GraphicsUnit.Pixel);
        }
    }
}

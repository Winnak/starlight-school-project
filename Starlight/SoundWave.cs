﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace Starlight
{
    public enum SoundState
    {
        Play, Plays, Played, Fuck
    }

    class SoundWave : GameObject, IDisposable
    {
        private float intensity;
        private float time;
        private SizeF size;
        private SoundState state;
        private GraphicsPath path;

        public float Intensity
        {
            get { return this.intensity; }
            set { this.intensity = value; }
        }
        public SizeF Size
        {
            get { return this.size; }
            set { this.size = value; }
        }
        public SoundState State
        {
            get { return this.state; }
            set { this.state = value; }
        }
        public GraphicsPath Path
        {
            get { return this.path; }
            set { this.path = value; }
        }


        public SoundWave(PointF position, float intensity) : base(position)
        {
            this.time = 0;
            this.intensity = intensity;
            this.state = SoundState.Plays;
            this.path = new GraphicsPath();
        }

        public override void Update(float fps)
        {
            if(this.state == SoundState.Plays)
            {
                time += 1 / fps;

                this.Size = new SizeF(time * 800, time * 800);

                path.AddEllipse(
                    new RectangleF(
                        new PointF(position.X - size.Width / 2,
                            position.Y - size.Height / 2),
                        size));

                if (time > intensity)
                {
                    this.state = SoundState.Played;
                }
            }
            else if(this.state == SoundState.Played)
            {
                path.Reset();
                Dispose();
                this.state = SoundState.Fuck;
            } else if(this.state == SoundState.Fuck)
            {
                //So we don't update like crazy
                return;
            }
        }
        public override void Draw(Graphics graphics)
        {
            graphics.DrawEllipse(new Pen(Brushes.Wheat, 10),
                new RectangleF(new PointF(position.X - size.Width / 2,
                    position.Y - size.Height / 2), size));
        }

        public void Dispose()
        {
            this.path.Dispose();
            Program.CurrentlyPlaying.Remove(this);
        }
    }
}
